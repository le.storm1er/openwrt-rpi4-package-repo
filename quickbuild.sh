#!/bin/bash

set -e

CWD=`pwd`
KERNEL_PARTSIZE=512
ROOTFS_PARTSIZE=4096
PACKAGES="\
    bash \
    busybox \
    collectd-mod-csv \
    collectd-mod-ping \
    collectd-mod-thermal \
    curl \
    dnsmasq \
    kmod-usb-net-rndis \
    kmod-usb-net-rtl8152 \
    luci \
    luci-app-adblock \
    luci-app-statistics \
    luci-app-wrtbwmon \
    luci-i18n-nft-qos-fr \
    luci-i18n-mwan3-fr \
    luci-i18n-nlbwmon-fr \
    tcpdump \
    uclient-fetch \
    kmod-ipt-nat6 \
    node-npm \
    python3 \
    make \
    coreutils-nohup \
    htop \
    procps-ng-pkill \
    rsync \
    git \
"

echo -e "\e[32m######### Ensure quickbuild folder...\e[39m"
mkdir -p ${CWD}/quickbuild

echo -e "\e[32m######### Cleaning quickbuild folder...\e[39m"
rm -rf ${CWD}/quickbuild/openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64

echo -e "\e[32m######### Downloading latest image builder...\e[39m"
read -p "Do you want to download latest image from downloads.openwrt.org? (expecting ${CWD}/quickbuild/openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64.tar.xz if not) [Yy]:" -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]
then
    cd ${CWD}/quickbuild
    rm -f openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64.tar.xz
    wget -q --show-progress https://downloads.openwrt.org/snapshots/targets/bcm27xx/bcm2711/openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64.tar.xz
fi

echo -e "\e[32m######### Extract image builder...\e[39m"
cd ${CWD}/quickbuild
tar -xf openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64.tar.xz

echo -e "\e[32m######### Downloading the current router config...\e[39m"
mkdir -p ${CWD}/quickbuild/openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64/files/etc/
read -p "Do you want to download current uci config? (using rsync root@openwrt.lan) [Yy]:" -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]
then
    rsync -r root@openwrt.lan:/etc/config ${CWD}/quickbuild/openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64/files/etc/
fi

echo -e "\e[32m######### Downloading the current node files...\e[39m"
mkdir -p ${CWD}/quickbuild/openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64/files/etc/
read -p "Do you want to download current node files? (using rsync root@openwrt.lan) [Yy]:" -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]
then
    rsync -rv root@openwrt.lan:/etc/node ${CWD}/quickbuild/openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64/files/etc/
fi

echo -e "\e[32m######### Custom files:\e[39m"
echo -e "\e[32mIf you need specific files into your build,\e[39m"
echo -e "\e[32mput them now into `pwd`/files/\e[39m"
echo -e "\e[32mPress any key to continue\e[39m"
read -n 1 -s -r -p ""

echo -e "\e[32m######### Preparing build...\e[39m"
cd ${CWD}/quickbuild/openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64
make clean
echo "src/gz custom https://gitlab.com/le.storm1er/openwrt-rpi4-package-repo/-/raw/master" >> repositories.conf
sed -i "/^option check_signature/d" repositories.conf
sed -i -E "s/CONFIG_TARGET_KERNEL_PARTSIZE\=[0-9]*/CONFIG_TARGET_KERNEL_PARTSIZE=${KERNEL_PARTSIZE}/g" .config
sed -i -E "s/CONFIG_TARGET_ROOTFS_PARTSIZE\=[0-9]*/CONFIG_TARGET_ROOTFS_PARTSIZE=${ROOTFS_PARTSIZE}/g" .config

echo -e "\e[32m######### Build...\e[39m"
make image FILES=files/ PACKAGES="${PACKAGES}"

echo -e "\e[32m######### Move builds...\e[39m"
mkdir -p ${CWD}/quickbuild/img
cp -rf ${CWD}/quickbuild/openwrt-imagebuilder-bcm27xx-bcm2711.Linux-x86_64/build_dir/target-aarch64_cortex-a72_musl/linux-bcm27xx_bcm2711/tmp/* ${CWD}/quickbuild/img
echo -e "\e[32mBuild is done: ${CWD}/quickbuild/img\e[39m"
