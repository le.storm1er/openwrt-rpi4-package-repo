# Openwrt custom repository for my RPI-4 openwrt

## Using with image builder

```bash
echo "src/gz custom https://gitlab.com/le.storm1er/openwrt-rpi4-package-repo/-/raw/master/" >> repositories.conf
```

Then add any packages you want from [list](https://gitlab.com/le.storm1er/openwrt-rpi4-package-repo/-/blob/master/Packages).

## Quick build image script

```bash
bash <(curl -s https://gitlab.com/le.storm1er/openwrt-rpi4-package-repo/-/raw/master/quickbuild.sh)
```

## Adding a new package to this repo

Download the latest image builder (see above), then:

```bash
cd ${IMAGE_BUILDER_DIR}
git clone git@gitlab.com:le.storm1er/openwrt-rpi4-package-repo.git packages
cd packages
# Add *.ipk packages in this folder
cd ..
make package_index
cd packages
git add .
git commit -m "Added awesome package"
```
